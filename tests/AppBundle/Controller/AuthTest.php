<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/api/todo/');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());

        $client->request('GET', '/api/todo/', [], [], ['HTTP_AUTHORIZATION' => 'Basic YWRtaW46YWRtaW4xMjM=']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
