<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/7/17
 * Time: 17:00
 */

namespace AppBundle\Entity;


class Priority
{
    const LOWER = 1;
    const MEDIUM = 2;
    const HIGH = 3;
}