<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/7/17
 * Time: 16:42
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Priority;
use AppBundle\Entity\ToDo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api/todo")
 */
class TodoApiController extends FOSRestController
{
    /**
     * @Rest\Get("/")
     */
    public function getAction(Request $request)
    {
        $limit = $request->get("limit", 10);
        $page = $request->get("page", 0);

        $result = $this->getDoctrine()->getRepository('AppBundle:ToDo')->findActive($limit, $page);
        if ($result === null) {
            return new JsonResponse("", Response::HTTP_NOT_FOUND);
        }

        return $result;
    }

    /**
     * @Rest\Get("/search")
     */
    public function searchAction(Request $request)
    {
        $query = $request->get("query");

        $result = $this->getDoctrine()->getRepository('AppBundle:ToDo')->searchAll($query);
        if ($result === null) {
            return new JsonResponse("", Response::HTTP_NOT_FOUND);
        }

        return $result;
    }

    /**
     * @Rest\Get("/searchByTitle")
     */
    public function searchByTitleAction(Request $request)
    {
        $query = $request->get("query");

        $result = $this->getDoctrine()->getRepository('AppBundle:ToDo')->findAllByTitle($query);
        if ($result === null) {
            return new JsonResponse("", Response::HTTP_NOT_FOUND);
        }

        return $result;
    }

    /**
     * @Rest\Get("/{id}")
     */
    public function idAction(int $id)
    {
        $result = $this->getDoctrine()->getRepository('AppBundle:ToDo')->find($id);
        if ($result === null) {
            return new JsonResponse("", Response::HTTP_NOT_FOUND);
        }

        return $result;
    }

    /**
     * @Rest\Post("/")
     */
    public function postAction(Request $request)
    {
        $title = $request->get('title');
        $content = $request->get('content');
        $priority = $request->get('priority', Priority::MEDIUM);
        $dueDate = $request->get('dueDate');

        if (empty($title) || empty($content)) {
            return new JsonResponse("", Response::HTTP_NOT_ACCEPTABLE);
        }

        $todo = new ToDo();
        $todo->setTitle($title);
        $todo->setContent($content);
        $todo->setPriority($priority);
        $todo->setCreationDate(new \DateTime());
        $todo->setDueDate($dueDate);

        $em = $this->getDoctrine()->getManager();
        $em->persist($todo);
        $em->flush();

        return $todo;
    }

    /**
     * @Rest\Put("/{id}")
     */
    public function updateAction(int $id, Request $request)
    {
        $title = $request->get('title');
        $content = $request->get('content');
        $priority = $request->get('priority', Priority::MEDIUM);
        $dueDate = $request->get('dueDate');

        if (empty($title) || empty($content)) {
            return new JsonResponse("", Response::HTTP_NOT_ACCEPTABLE);
        }

        $sn = $this->getDoctrine()->getManager();
        $todo = $this->getDoctrine()->getRepository('AppBundle:ToDo')->find($id);
        if (empty($todo)) {
            return new JsonResponse("", Response::HTTP_NOT_FOUND);
        }

        $todo->setTitle($title);
        $todo->setContent($content);
        $todo->setPriority($priority);
        $todo->setCreationDate(new \DateTime());
        $todo->setDueDate($dueDate);
        $todo->setModificationDate(new \DateTime());
        $sn->flush();

        return $todo;
    }

    /**
     * @Rest\Delete("/{id}")
     */
    public function deleteAction(int $id)
    {
        $sn = $this->getDoctrine()->getManager();
        $todo = $this->getDoctrine()->getRepository('AppBundle:ToDo')->find($id);
        if (empty($todo)) {
            return new JsonResponse("", Response::HTTP_NOT_FOUND);
        }

        $todo->setIsDeleted(true);
        $todo->setDeleteDate(new \DateTime());
        $sn->flush();

        return new JsonResponse("", Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/{id}/done")
     */
    public function doneAction(int $id)
    {
        $sn = $this->getDoctrine()->getManager();
        $todo = $this->getDoctrine()->getRepository('AppBundle:ToDo')->find($id);
        if (empty($todo)) {
            return new JsonResponse("", Response::HTTP_NOT_FOUND);
        }

        $todo->setIsDone(true);
        $todo->setModificationDate(new \DateTime());
        $sn->flush();

        return $todo;
    }
}